require ("isomorphic-fetch")
import React from "react";
import {getInfo} from '../lib/utils.js';
import Layout from '../components/Layout.js';

const buttonHeight = "25px"

const imgStyle = {
  height: "80px",
  marginTop: "20px"
}
const textStyle = {
  fontFamily: "Arial"
}
const returnedTextStyle = {
    fontFamily: "Avantgarde, sans-serif"
}
const buttonStyle = {
  margin: "0 auto",
  cursor: "pointer",
  backgroundColor: "#9ee6c9",
  color: "#000000",
  height: buttonHeight,
  width: "90px",
  verticalAlign: "middle",
  horizontalAlign: "middle",
  lineHeight: buttonHeight,
  border: "2px solid #75bda0",
  fontFamily: "Arial"
}
const tableStyle = {
  margin: "0 auto",
  width: "100%",
  align: "center",
  padding: "5px",
  borderCollapse: "separate",
  borderBottom: "1px solid black"

}

  class FindCompnent extends React.Component {
    constructor(props) {
      super(props);
      this.state={search: ""}
  }

  handleUpdate(evt){
    this.setState({search: evt.target.value});
  }

  async handleSearch(evt) {
      const place = await getInfo(this.state.search);
      console.log(place)
      this.setState({place});
  }

  render() {
    return (
      <Layout>
      <div style={{ margin: "auto auto", width: "600px", textAlign: "center"}}>
        <img src="/static/world.png" alt="The Earth" className="App-logo" style={imgStyle}/>
        <h2>Where in the World is Maria?</h2>
        <p style={textStyle}><input type='text' value={this.state.search} onChange={this.handleUpdate.bind(this)} /></p>
        <div className="button" onClick={this.handleSearch.bind(this)} style={buttonStyle}>Search</div>

        {("place" in this.state && this.state.place.genResponse.length > 0) ?
        <div>
          {(this.state.place.genResponse[0].cityname == this.state.search) ?
          <div>
            <br />
            <h1>Looks like she's in {this.state.place.genResponse[0].cityname}, {this.state.place.genResponse[0].countryname}!</h1>
            <h3>Selected information about {this.state.place.genResponse[0].cityname}:</h3>
            In {this.state.place.genResponse[0].cityname} they speak {this.state.place.langResponse[0].languages} and have a population
            of {this.state.place.genResponse[0].citypopulation} people! {this.state.place.genResponse[0].cityname} is
            located in the {this.state.place.genResponse[0].region} region
            of {this.state.place.genResponse[0].countryname} on the continent of {this.state.place.genResponse[0].continent}.
          </div> : null}

          {(this.state.place.genResponse[0].countryname == this.state.search || this.state.place.genResponse[0].countrycode == this.state.search) ?
          <div>
            <br />
            <h1>Looks like she's in {this.state.place.genResponse[0].countryname}!</h1>
            <h3>Selected {this.state.place.genResponse[0].countryname} Statistics and Info:</h3>
            Continent: {this.state.place.genResponse[0].continent}
            <br />
            Local Name: {this.state.place.genResponse[0].localname}
            <br />
            Head of State: {this.state.place.genResponse[0].headofstate}
            <br />
            Government Type: {this.state.place.genResponse[0].governmentform}
            <br />
            Total Population: {this.state.place.genResponse[0].countrypopulation}
            <br />
            Spoken Languages: {this.state.place.langResponse[0].languages}
            <br />
            <h3>Selected City Statistics:</h3>
            <table style={tableStyle}>
            <thead>
            <th>City</th>
            <th>District</th>
            <th>Population</th>
            </thead>
            <tbody>
            {this.state.place.genResponse.map(function(item) {
              return(
              <tr>
                <td>{item.cityname}</td>
                <td>{item.district}</td>
                <td>{item.citypopulation}</td>
              </tr>
              )
              })
            }
            </tbody>
            </table>
          </div> : null}

          {(this.state.place.genResponse[0].continent == this.state.search) ?
          <div>
            <br />
            <h1>Looks like she's in {this.state.place.genResponse[0].continent}!</h1>
            Here's what they speak all across {this.state.place.genResponse[0].continent}: <br /> {this.state.place.langResponse[0].languages}
            <h3>Selected Country Statistics:</h3>
            <table style={tableStyle}>
            <thead>
            <th>Country</th>
            <th>Population</th>
            <th>Life Expectancy</th>
            </thead>
            <tbody>
            {this.state.place.genResponse.map(function(item) {
              return(
              <tr>
                <td>{item.countryname}</td>
                <td>{item.countrypopulation}</td>
                <td>{item.lifeexpectancy} Years</td>
              </tr>
              )
              })
            }
            </tbody>
            </table>
          </div> : null}

          {("error" in this.state.place.genResponse[0]) ?
          <div>
            <br />
            {this.state.place.genResponse[0].error}
          </div> : null}
        </div> : null}

        <style jsx>{`

          .description {
            font-family: "Arial";
            font-size: "10px";
          }

          ul {
            padding: 0;
          }

          li {
            list-style: none;
            margin: 5px 0;
          }
          a {
            text-decoration: none;
            color: blue;
          }

          a:hover {
            opacity: 0.6;
          }
        `}</style>
      </div>
      </Layout>
    );
  }
}

export default FindCompnent;

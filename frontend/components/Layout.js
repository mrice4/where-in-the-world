const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #000000',
  backgroundColor: '#bce4eb',

}

export default function Layout(props) {
  return (
    <div style={layoutStyle}>
      {props.children}
    </div>
  )
}

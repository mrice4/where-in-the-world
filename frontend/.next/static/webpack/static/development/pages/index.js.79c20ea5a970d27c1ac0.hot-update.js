webpackHotUpdate("static/development/pages/index.js",{

/***/ "./lib/utils.js":
/*!**********************!*\
  !*** ./lib/utils.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

var _bluebird = _interopRequireDefault(__webpack_require__(/*! bluebird */ "./node_modules/bluebird/js/browser/bluebird.js"));

__webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

function handleError(err) {
  console.warn(err);
  return null;
}

;

function getPlaceInfo(place) {
  return fetch("http://localhost:5000/api/countries?q=".concat(place)).then(function (resp) {
    return resp.json();
  });
}

module.exports = {
  getInfo: function getInfo(value) {
    if (value != "") {
      var resp = getPlaceInfo(value)["catch"](handleError);

      if (resp == null) {
        var ret = {
          genResponse: []
        };
        return ret;
      } else {
        return resp;
      }
    } else {
      var _ret = {
        genResponse: []
      };
      return _ret;
    }
  }
};

/***/ })

})
//# sourceMappingURL=index.js.79c20ea5a970d27c1ac0.hot-update.js.map
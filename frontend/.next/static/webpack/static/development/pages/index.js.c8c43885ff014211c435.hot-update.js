webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../lib/utils.js */ "./lib/utils.js");
/* harmony import */ var _lib_utils_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_lib_utils_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_Layout_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/Layout.js */ "./components/Layout.js");







var _jsxFileName = "/mnt/c/Users/devlk/OneDrive/Documents/Databases/WhereInTheWorld/frontend/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;

__webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");




var buttonHeight = "25px";
var imgStyle = {
  height: "80px",
  marginTop: "20px"
};
var textStyle = {
  fontFamily: "Arial"
};
var returnedTextStyle = {
  fontFamily: "Avantgarde, sans-serif"
};
var buttonStyle = {
  margin: "0 auto",
  cursor: "pointer",
  backgroundColor: "#9ee6c9",
  color: "#000000",
  height: buttonHeight,
  width: "90px",
  verticalAlign: "middle",
  horizontalAlign: "middle",
  lineHeight: buttonHeight,
  border: "2px solid #75bda0",
  fontFamily: "Arial"
};
var tableStyle = {
  margin: "0 auto",
  width: "100%",
  align: "center",
  padding: "5px",
  borderCollapse: "separate",
  borderBottom: "1px solid black"
};

var FindCompnent =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(FindCompnent, _React$Component);

  function FindCompnent(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, FindCompnent);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(FindCompnent).call(this, props));
    _this.state = {
      search: ""
    };
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(FindCompnent, [{
    key: "handleUpdate",
    value: function handleUpdate(evt) {
      this.setState({
        search: evt.target.value
      });
    }
  }, {
    key: "handleSearch",
    value: function () {
      var _handleSearch = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(evt) {
        var place;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return Object(_lib_utils_js__WEBPACK_IMPORTED_MODULE_9__["getInfo"])(this.state.search);

              case 2:
                place = _context.sent;
                console.log(place);
                this.setState({
                  place: place
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function handleSearch(_x) {
        return _handleSearch.apply(this, arguments);
      }

      return handleSearch;
    }()
  }, {
    key: "render",
    value: function render() {
      return __jsx(_components_Layout_js__WEBPACK_IMPORTED_MODULE_10__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, __jsx("div", {
        style: {
          margin: "auto auto",
          width: "600px",
          textAlign: "center"
        },
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, __jsx("img", {
        src: "/static/world.png",
        alt: "The Earth",
        style: imgStyle,
        className: "jsx-1455412518" + " " + "App-logo",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }), __jsx("h2", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }, "Where in the World is Maria?"), __jsx("p", {
        style: textStyle,
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      }, __jsx("input", {
        type: "text",
        value: this.state.search,
        onChange: this.handleUpdate.bind(this),
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      })), __jsx("div", {
        onClick: this.handleSearch.bind(this),
        style: buttonStyle,
        className: "jsx-1455412518" + " " + "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, "Search"), "place" in this.state && this.state.place.genResponse.length > 0 ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }, this.state.place.genResponse[0].cityname == this.state.search ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }, __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }), "Looks like she's in ", this.state.place.genResponse[0].cityname, ", ", this.state.place.genResponse[0].countryname, "!", __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }), "Here's some cool information about ", this.state.place.genResponse[0].cityname, ":", __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }), "In Kabul they speak ", this.state.place.langResponse[0].languages, " and have a population of", this.state.place.genResponse[0].citypopulation, " people! ", this.state.place.genResponse[0].cityname, "is located in the ", this.state.place.genResponse[0].region, " region of ", this.state.place.genResponse[0].countryname, "on the continent of ", this.state.place.genResponse[0].continent, ".") : null, this.state.place.genResponse[0].countryname == this.state.search ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        },
        __self: this
      }) : null, this.state.place.genResponse[0].countrycode == this.state.search ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }) : null, this.state.place.genResponse[0].continent == this.state.search ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }) : null, "error" in this.state.place.genResponse[0] ? __jsx("div", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }, this.state.place.genResponse[0].error) : null, __jsx("br", {
        className: "jsx-1455412518",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      })) : null, __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default.a, {
        id: "1455412518",
        __self: this
      }, ".description.jsx-1455412518{font-family:\"Arial\";font-size:\"10px\";}ul.jsx-1455412518{padding:0;}li.jsx-1455412518{list-style:none;margin:5px 0;}a.jsx-1455412518{-webkit-text-decoration:none;text-decoration:none;color:blue;}a.jsx-1455412518:hover{opacity:0.6;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9tbnQvYy9Vc2Vycy9kZXZsay9PbmVEcml2ZS9Eb2N1bWVudHMvRGF0YWJhc2VzL1doZXJlSW5UaGVXb3JsZC9mcm9udGVuZC9wYWdlcy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFnR29CLEFBSWlDLEFBS1YsQUFJTSxBQUlLLEFBS1QsVUFaZCxFQWFBLElBVGUsSUFUSSxTQVVuQixRQVRBLGFBWWEsV0FDYiIsImZpbGUiOiIvbW50L2MvVXNlcnMvZGV2bGsvT25lRHJpdmUvRG9jdW1lbnRzL0RhdGFiYXNlcy9XaGVyZUluVGhlV29ybGQvZnJvbnRlbmQvcGFnZXMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJyZXF1aXJlIChcImlzb21vcnBoaWMtZmV0Y2hcIilcclxuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQge2dldEluZm99IGZyb20gJy4uL2xpYi91dGlscy5qcyc7XHJcbmltcG9ydCBMYXlvdXQgZnJvbSAnLi4vY29tcG9uZW50cy9MYXlvdXQuanMnO1xyXG5cclxuY29uc3QgYnV0dG9uSGVpZ2h0ID0gXCIyNXB4XCJcclxuXHJcbmNvbnN0IGltZ1N0eWxlID0ge1xyXG4gIGhlaWdodDogXCI4MHB4XCIsXHJcbiAgbWFyZ2luVG9wOiBcIjIwcHhcIlxyXG59XHJcbmNvbnN0IHRleHRTdHlsZSA9IHtcclxuICBmb250RmFtaWx5OiBcIkFyaWFsXCJcclxufVxyXG5jb25zdCByZXR1cm5lZFRleHRTdHlsZSA9IHtcclxuICAgIGZvbnRGYW1pbHk6IFwiQXZhbnRnYXJkZSwgc2Fucy1zZXJpZlwiXHJcbn1cclxuY29uc3QgYnV0dG9uU3R5bGUgPSB7XHJcbiAgbWFyZ2luOiBcIjAgYXV0b1wiLFxyXG4gIGN1cnNvcjogXCJwb2ludGVyXCIsXHJcbiAgYmFja2dyb3VuZENvbG9yOiBcIiM5ZWU2YzlcIixcclxuICBjb2xvcjogXCIjMDAwMDAwXCIsXHJcbiAgaGVpZ2h0OiBidXR0b25IZWlnaHQsXHJcbiAgd2lkdGg6IFwiOTBweFwiLFxyXG4gIHZlcnRpY2FsQWxpZ246IFwibWlkZGxlXCIsXHJcbiAgaG9yaXpvbnRhbEFsaWduOiBcIm1pZGRsZVwiLFxyXG4gIGxpbmVIZWlnaHQ6IGJ1dHRvbkhlaWdodCxcclxuICBib3JkZXI6IFwiMnB4IHNvbGlkICM3NWJkYTBcIixcclxuICBmb250RmFtaWx5OiBcIkFyaWFsXCJcclxufVxyXG5jb25zdCB0YWJsZVN0eWxlID0ge1xyXG4gIG1hcmdpbjogXCIwIGF1dG9cIixcclxuICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgYWxpZ246IFwiY2VudGVyXCIsXHJcbiAgcGFkZGluZzogXCI1cHhcIixcclxuICBib3JkZXJDb2xsYXBzZTogXCJzZXBhcmF0ZVwiLFxyXG4gIGJvcmRlckJvdHRvbTogXCIxcHggc29saWQgYmxhY2tcIlxyXG5cclxufVxyXG5cclxuICBjbGFzcyBGaW5kQ29tcG5lbnQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcclxuICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgICB0aGlzLnN0YXRlPXtzZWFyY2g6IFwiXCJ9XHJcbiAgfVxyXG5cclxuICBoYW5kbGVVcGRhdGUoZXZ0KXtcclxuICAgIHRoaXMuc2V0U3RhdGUoe3NlYXJjaDogZXZ0LnRhcmdldC52YWx1ZX0pO1xyXG4gIH1cclxuXHJcbiAgYXN5bmMgaGFuZGxlU2VhcmNoKGV2dCkge1xyXG4gICAgICBjb25zdCBwbGFjZSA9IGF3YWl0IGdldEluZm8odGhpcy5zdGF0ZS5zZWFyY2gpO1xyXG4gICAgICBjb25zb2xlLmxvZyhwbGFjZSlcclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7cGxhY2V9KTtcclxuICB9XHJcblxyXG4gIHJlbmRlcigpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxMYXlvdXQ+XHJcbiAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luOiBcImF1dG8gYXV0b1wiLCB3aWR0aDogXCI2MDBweFwiLCB0ZXh0QWxpZ246IFwiY2VudGVyXCJ9fT5cclxuICAgICAgICA8aW1nIHNyYz1cIi9zdGF0aWMvd29ybGQucG5nXCIgYWx0PVwiVGhlIEVhcnRoXCIgY2xhc3NOYW1lPVwiQXBwLWxvZ29cIiBzdHlsZT17aW1nU3R5bGV9Lz5cclxuICAgICAgICA8aDI+V2hlcmUgaW4gdGhlIFdvcmxkIGlzIE1hcmlhPzwvaDI+XHJcbiAgICAgICAgPHAgc3R5bGU9e3RleHRTdHlsZX0+PGlucHV0IHR5cGU9J3RleHQnIHZhbHVlPXt0aGlzLnN0YXRlLnNlYXJjaH0gb25DaGFuZ2U9e3RoaXMuaGFuZGxlVXBkYXRlLmJpbmQodGhpcyl9IC8+PC9wPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uXCIgb25DbGljaz17dGhpcy5oYW5kbGVTZWFyY2guYmluZCh0aGlzKX0gc3R5bGU9e2J1dHRvblN0eWxlfT5TZWFyY2g8L2Rpdj5cclxuXHJcbiAgICAgICAgeyhcInBsYWNlXCIgaW4gdGhpcy5zdGF0ZSAmJiB0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlLmxlbmd0aCA+IDApID8gPGRpdj5cclxuICAgICAgICAgIHsodGhpcy5zdGF0ZS5wbGFjZS5nZW5SZXNwb25zZVswXS5jaXR5bmFtZSA9PSB0aGlzLnN0YXRlLnNlYXJjaCkgPyA8ZGl2PlxyXG4gICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgTG9va3MgbGlrZSBzaGUncyBpbiB7dGhpcy5zdGF0ZS5wbGFjZS5nZW5SZXNwb25zZVswXS5jaXR5bmFtZX0sIHt0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLmNvdW50cnluYW1lfSFcclxuICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgIEhlcmUncyBzb21lIGNvb2wgaW5mb3JtYXRpb24gYWJvdXQge3RoaXMuc3RhdGUucGxhY2UuZ2VuUmVzcG9uc2VbMF0uY2l0eW5hbWV9OlxyXG4gICAgICAgICAgICA8YnIgLz5cclxuICAgICAgICAgICAgSW4gS2FidWwgdGhleSBzcGVhayB7dGhpcy5zdGF0ZS5wbGFjZS5sYW5nUmVzcG9uc2VbMF0ubGFuZ3VhZ2VzfSBhbmQgaGF2ZSBhIHBvcHVsYXRpb24gb2ZcclxuICAgICAgICAgICAge3RoaXMuc3RhdGUucGxhY2UuZ2VuUmVzcG9uc2VbMF0uY2l0eXBvcHVsYXRpb259IHBlb3BsZSEge3RoaXMuc3RhdGUucGxhY2UuZ2VuUmVzcG9uc2VbMF0uY2l0eW5hbWV9XHJcbiAgICAgICAgICAgIGlzIGxvY2F0ZWQgaW4gdGhlIHt0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLnJlZ2lvbn0gcmVnaW9uIG9mIHt0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLmNvdW50cnluYW1lfVxyXG4gICAgICAgICAgICBvbiB0aGUgY29udGluZW50IG9mIHt0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLmNvbnRpbmVudH0uXHJcbiAgICAgICAgICA8L2Rpdj4gOiBudWxsfVxyXG4gICAgICAgICAgeyh0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLmNvdW50cnluYW1lID09IHRoaXMuc3RhdGUuc2VhcmNoKSA/IDxkaXY+XHJcblxyXG4gICAgICAgICAgIDwvZGl2PiA6IG51bGx9XHJcbiAgICAgICAgICB7KHRoaXMuc3RhdGUucGxhY2UuZ2VuUmVzcG9uc2VbMF0uY291bnRyeWNvZGUgPT0gdGhpcy5zdGF0ZS5zZWFyY2gpID8gPGRpdj5cclxuXHJcbiAgICAgICAgICA8L2Rpdj4gOiBudWxsfVxyXG4gICAgICAgICAgeyh0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLmNvbnRpbmVudCA9PSB0aGlzLnN0YXRlLnNlYXJjaCkgPyA8ZGl2PlxyXG5cclxuICAgICAgICAgIDwvZGl2PiA6IG51bGx9XHJcbiAgICAgICAgICB7KFwiZXJyb3JcIiBpbiB0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdKSA/IDxkaXY+XHJcbiAgICAgICAgICAgIHt0aGlzLnN0YXRlLnBsYWNlLmdlblJlc3BvbnNlWzBdLmVycm9yfVxyXG4gICAgICAgICAgPC9kaXY+IDogbnVsbH1cclxuXHJcbiAgICAgICAgICA8YnIgLz5cclxuXHJcbiAgICAgICAgICA8L2Rpdj4gOiBudWxsfVxyXG5cclxuXHJcblxyXG4gICAgICAgIDxzdHlsZSBqc3g+e2BcclxuXHJcbiAgICAgICAgICAuZGVzY3JpcHRpb24ge1xyXG4gICAgICAgICAgICBmb250LWZhbWlseTogXCJBcmlhbFwiO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IFwiMTBweFwiO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHVsIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBsaSB7XHJcbiAgICAgICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICAgICAgICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICBjb2xvcjogYmx1ZTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBhOmhvdmVyIHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMC42O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIGB9PC9zdHlsZT5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDwvTGF5b3V0PlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEZpbmRDb21wbmVudDtcclxuIl19 */\n/*@ sourceURL=/mnt/c/Users/devlk/OneDrive/Documents/Databases/WhereInTheWorld/frontend/pages/index.js */")));
    }
  }]);

  return FindCompnent;
}(react__WEBPACK_IMPORTED_MODULE_8___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (FindCompnent);

/***/ })

})
//# sourceMappingURL=index.js.c8c43885ff014211c435.hot-update.js.map
webpackHotUpdate("static/development/pages/index.js",{

/***/ "./lib/utils.js":
/*!**********************!*\
  !*** ./lib/utils.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime-corejs2/helpers/interopRequireDefault */ "./node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault.js");

var _readOnlyError2 = _interopRequireDefault(__webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/readOnlyError */ "./node_modules/@babel/runtime-corejs2/helpers/esm/readOnlyError.js"));

var _bluebird = _interopRequireDefault(__webpack_require__(/*! bluebird */ "./node_modules/bluebird/js/browser/bluebird.js"));

__webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

function handleError(err) {
  console.warn(err);
  return null;
}

;

function getPlaceInfo(place) {
  return fetch("http://localhost:5000/api/countries?q=".concat(place)).then(function (resp) {
    return resp.json();
  });
}

module.exports = {
  getInfo: function getInfo(value) {
    if (value != "") {
      var resp = getPlaceInfo(value)["catch"](handleError);
      console.log(resp);

      if (resp = ((0, _readOnlyError2["default"])("resp"), null)) {
        var ret = {
          genResponse: []
        };
        return ret;
      } else {
        return resp;
      }
    } else {
      var _ret = {
        genResponse: []
      };
      return _ret;
    }
  }
};

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/readOnlyError.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/readOnlyError.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _readOnlyError; });
function _readOnlyError(name) {
  throw new Error("\"" + name + "\" is read-only");
}

/***/ })

})
//# sourceMappingURL=index.js.c464e3533e749cdd02a4.hot-update.js.map
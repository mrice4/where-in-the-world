require("isomorphic-fetch");
import BPromise from "bluebird";

function handleError(err){
  console.warn(err);
  return null;
};

function getPlaceInfo(place){
  return fetch(`http://35.188.250.100/api/countries?q=${place}`).then(function(resp){
    return resp.json();
  })
}

module.exports = {
  getInfo: function(value){
    if (value != ""){
        return getPlaceInfo(value).catch(handleError)
    }
    else {
      const ret = {genResponse: [{error: "You didn't type anything in!"}]};
      return ret;
    }
  }
};

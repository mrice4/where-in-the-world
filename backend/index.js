const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

//app.set("port", 8080);
app.set("port", 8080)
app.use(bodyParser.json({type:"application/json"}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

const Pool = require("pg").Pool;
const config = {
  host: "localhost",
  user: "world",
  password: "Th3W0rmTurn5",
  database: "world"
};

const pool = new Pool(config);

app.get("/api/countries", async(req, res) => {
  const search = req.query.q;
  try {
    const genRequest = "SELECT DISTINCT city.name AS cityname, city.countrycode, district, " +
      "city.population AS citypopulation, country.name AS countryname, continent, region, " +
      "surfacearea, indepyear, country.population AS countrypopulation, lifeexpectancy, " +
      "gnp, localname, governmentform, headofstate, capital " +
      "FROM city JOIN country on city.countrycode = country.code JOIN " +
      "countrylanguage on country.code = countrylanguage.countrycode WHERE city.name LIKE $1 OR " +
      "country.name LIKE $1 OR country.code LIKE $1 OR country.continent LIKE $1 LIMIT 20"
    const genResponse = await pool.query(genRequest, [search]);
    const langRequest = "SELECT string_agg(DISTINCT language, ', ') AS languages " +
    "FROM city JOIN country on city.countrycode = country.code JOIN " +
    "countrylanguage on country.code = countrylanguage.countrycode WHERE city.name LIKE $1 OR " +
    "country.name LIKE $1 OR country.code LIKE $1 OR country.continent LIKE $1"
    const langResponse = await pool.query(langRequest, [search]);
    if (genResponse.rowCount == 0) {
      res.json({genResponse : [{error: "Your search wasn't found! Make sure read the search requirements."}]})
    } else {
      res.json({genResponse: genResponse.rows, langResponse: langResponse.rows});
    }
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    res.json({error: "unforseen error occured listing attendees, please contact builder"})
  }
});

app.listen(app.get("port"), () => {
  console.log(`Find the server at http://localhost:${app.get("port")}`);
});
